######################################
# DMT MODEM SIMULATION SOFTWARE      #
#   Project Makefile                 #
#                                    #
# David Valero Jiménez               #
# University of Castilla - La Mancha #
# Spain, 2015                        #
######################################

.SUFFIXES: .o .c

CC=gcc
BDG=gdb
RM=rm -frv
MKDIR=mkdir -p
ECHO=echo


# This will be assingned by Makefile.32 or Makefile.64 to force architecture
ARCH=

CFLAGS=-c -Wall $(ARCH)
LDFLAGS=-static $(ARCH)
DBGFLAGS=-ggdb

LIBS=-lm


# DIRECTORIES DEFINITION

SRCDIR=src/
INCDIR=include/
OBJDIR=obj/
BINDIR=bin/

DIRS=$(SRCDIR) $(INCDIR) $(OBJDIR) $(BINDIR)


# NAMES DEFINITION

NM_TX=simu-tx
NM_RX=simu-rx
NM_STATS=simu-stats
NM_CMPLX=complex_signal
NM_DATA=data
NM_SIMU=simulation
NM_CMAP=constell_mapping
NM_DFT=dft
NM_FFT=fft
NM_CLI=cli
NM_MISC=signalproc_misc
NM_PLOT=gnuplot

BINNAME_TX=$(BINDIR)/$(NM_TX)
BINNAME_RX=$(BINDIR)/$(NM_RX)
BINNAME_STATS=$(BINDIR)/$(NM_STATS)


# OBJECT CODE

OBJS_COMMON=$(NM_CMPLX).o $(NM_DATA).o $(NM_SIMU).o $(NM_CMAP).o $(NM_DFT).o $(NM_FFT).o $(NM_CLI).o $(NM_MISC).o

_OBJS_TX=$(OBJS_COMMON) $(NM_PLOT).o $(NM_TX).o
OBJS_TX=$(patsubst %,$(OBJDIR)%,$(_OBJS_TX))

_OBJS_RX=$(OBJS_COMMON) $(NM_RX).o
OBJS_RX=$(patsubst %,$(OBJDIR)%,$(_OBJS_RX))

_OBJS_STATS=$(OBJS_COMMON) $(NM_STATS).o
OBJS_STATS=$(patsubst %,$(OBJDIR)%,$(_OBJS_STATS))


# TARGET DEFINITION

all: $(DIRS) $(BINNAME_TX) $(BINNAME_RX) $(BINNAME_STATS)

debug: echodebug
debug: CFLAGS += $(DBGFLAGS)
debug: all

$(BINNAME_TX): $(OBJS_TX)
	@$(ECHO) Linking $(BINNAME_TX)...
	$(CC) $(LDFLAGS) $(OBJS_TX) $(LIBS) -o $@

$(BINNAME_RX): $(OBJS_RX)
	@$(ECHO) Linking $(BINNAME_RX)...
	$(CC) $(LDFLAGS) $(OBJS_RX) $(LIBS) -o $@

$(BINNAME_STATS): $(OBJS_STATS)
	@$(ECHO) Linking $(BINNAME_STATS)...
	$(CC) $(LDFLAGS) $(OBJS_STATS) $(LIBS) -lrt -o $@

$(OBJDIR)%.o: $(SRCDIR)%.c $(INCDIR)%.h
	$(CC) $(CFLAGS) -I$(INCDIR) $< -o $@


.PHONY: all clean debug echodebug

$(DIRS):
	@$(ECHO) Making directories $(OBJDIR) and $(BINDIR).
	@$(ECHO) Assembling code...
	@for dir in $(DIRS); do \
		$(MKDIR) $$dir; \
	done

clean:
	$(RM) core
	$(RM) $(BINDIR)
	$(RM) $(OBJDIR)
	$(RM) $(INCDIR)*~
	$(RM) $(SRCDIR)*~
	$(RM) ./*~

echodebug:
	@$(ECHO) "Compiling object code with debugging information..."



