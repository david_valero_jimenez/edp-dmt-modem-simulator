set style line 1 pt 2 lc rgb "cyan"
set style line 2 pt 12 lc rgb "red"

set xzeroaxis

set xlabel 'samples'
set ylabel 'amplitude'

set multiplot layout 2,1

set title 'Time-domain signal'
plot signal every ::5 using 0:1 with impulses ls 2 title 'Noisy signal', \
     signal every ::5 using 0:1 with points ls 2 notitle, \
     noise using 0:1 with impulses ls 1 title 'Clean signal', \
     noise using 0:1 with points ls 1 notitle

set title 'Noise signal'
plot noise using 0:2 with impulses ls 2 title 'Isolated noise', \
     noise using 0:2 with points ls 2 notitle

unset multiplot
