set style line 1 pt 4 lc rgb "red"

set xzeroaxis

set xlabel 'samples'
set ylabel 'amplitude'

set title 'Time-domain signal'
plot signal every ::5 using 0:1 with impulses ls 1 title 'x(n)', \
     signal every ::5 using 0:1 with points ls 1 notitle
