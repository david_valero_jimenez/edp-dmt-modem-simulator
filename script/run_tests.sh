#!/bin/sh
echo -n .
bin/simu-tx -i examples/test1.dat -o /dev/null > results/t1.dat
echo -n .
bin/simu-tx -i examples/test2.dat -o /dev/null > results/t2.dat
echo -n .
bin/simu-tx -i examples/test3.dat -o /dev/null > results/t3.dat
echo -n .
bin/simu-tx -i examples/test4.dat -o /dev/null > results/t4.dat
echo -n .
bin/simu-tx -i examples/test5.dat -o /dev/null > results/t5.dat
echo -n .
bin/simu-stats -i examples/test5.dat -n 1 -t 10000 > results/t5_noise1.dat
echo -n .
bin/simu-stats -i examples/test5.dat -n 3 -t 10000 > results/t5_noise3.dat
echo -n .
bin/simu-stats -i examples/test5.dat -n 5 -t 10000 > results/t5_noise5.dat
echo -n .
bin/simu-tx -i examples/test6.dat -o /dev/null > results/t6.dat
echo -n .
bin/simu-stats -i examples/test6.dat -n 1 -t 10000 > results/t6_noise1.dat
echo -n .
bin/simu-stats -i examples/test6.dat -n 3 -t 10000 > results/t6_noise3.dat
echo -n .
bin/simu-stats -i examples/test6.dat -n 5 -t 10000 > results/t6_noise5.dat
echo -n .
bin/simu-stats -i examples/test6.dat -n 8 -t 10000 > results/t6_noise7.dat
echo -n .
bin/simu-tx -i examples/test7.dat -o /dev/null > results/t7.dat
echo -n .
bin/simu-stats -i examples/test7.dat -n 1 -t 10000 > results/t7_noise1.dat
echo -n .
bin/simu-stats -i examples/test7.dat -n 3 -t 10000 > results/t7_noise3.dat
echo -n .
bin/simu-stats -i examples/test7.dat -n 5 -t 10000 > results/t7_noise5.dat
echo -n .
bin/simu-stats -i examples/test7.dat -n 8 -t 10000 > results/t7_noise8.dat
echo -n .
bin/simu-stats -i examples/test7.dat -n 12 -t 10000 > results/t7_noise12.dat
echo -n .
bin/simu-tx -i examples/test8.dat -o /dev/null > results/t8.dat
echo

