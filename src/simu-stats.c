#include "simu-tx.h"


int main(int argc, char **argv) {
  simulation_data *d1, *d2;
  int i, n;
  double matches = 0, PVs = 0, PVn = 0, snr = 0;
  struct timespec t1, t2;


  /* Command line checking */
  cline_data cl_data;
  if(clinecheck_stats(argc, argv, &cl_data)) {
    help_stats();
    exit(1);
  }


  /* Configuration reading */
  d1 = read_input(&cl_data, TX);

  /* Generate transmission data */
  transmission_process(d1, 0);

  /* Run tests */
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
  for(i=0;i<cl_data.N_TESTS;i++) {

    /* Execute demodulation process */
    d2 = simu_clone(d1);
    csignal_free(d2->X);
    for(n=0;n<d2->N;n++) {
      d2->x->real[n] += d2->noise->real[n];
    }
    reception_process(d2, 0);

    /* Save statistics */
    if(d1->frame.data==d2->frame.data) {
      matches++;
    }
    PVs += d1->x_PEAK_AMP;
    PVn += d1->NOISE_PEAK_AMP;
    snr += d1->SNR_dB;

    /* Prepare next test */
    simu_free(d2, 1, 1, d2->NOISE_MAX_AMP>0?1:0);
    csignal_free(d1->noise);
    noise_simulation(d1, 0);

  }
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);


  /* Generate results */
  summary(d1, TX);
  printf("\nANALYSIS OF DMT MODEM BACKGROUND NOISE RESPONSE:\n");
  printf("  Percent of frame exact matches:\t\t%lg%% (%i of %i tests OK)\n",
         matches*100/cl_data.N_TESTS, (int)matches, cl_data.N_TESTS);
  printf("  Norm. signal average peak voltage:\t\t%lg V·NF (scale: %lg)\n",
         PVs/cl_data.N_TESTS, d1->scale!=0?d1->scale:1);
  printf("  Noise signal average peak voltage:\t\t%lg V·NF\n",
         PVn/cl_data.N_TESTS);
  printf("  Average Signal-to-Noise Ratio (SNR):\t\t%lg dB\n\n",
         snr/cl_data.N_TESTS);

  printf("Total simulation time: %lf s\n\n",
         (t2.tv_sec+(double)t2.tv_nsec/1000000000) -
         (t1.tv_sec+(double)t1.tv_nsec/1000000000));

  simu_free(d1, 1, 1, d1->NOISE_MAX_AMP>0?1:0);

  exit(0);
  return 0;
}

