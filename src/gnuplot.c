#include "gnuplot.h"


void generate_plot(char *ofile_name, simulation_data *d) {
  char *graf;
  int n;
  FILE *fnoise = NULL;

  printf("Generating GNUPLOT output...\n\n");

  if( d->noise != NULL ) {
    fnoise = fopen(NOISE_FILE, "w+");
    for(n=0;n<d->N;n++) {
      fprintf(fnoise, "%lf %lf\n", d->x->real[n], d->noise->real[n]);
    }
    fclose(fnoise);
    graf = (char*)malloc( sizeof(char) *
                          (36+strlen(NOISE_FILE)+strlen(SN_PLOT_FILE)+
                           strlen(ofile_name)) );
    sprintf(graf, "gnuplot -e 'signal=\"%s\";noise=\"" NOISE_FILE "\"' -p "
            SN_PLOT_FILE, ofile_name);
  } else {
    graf = (char*)malloc( sizeof(char) *
                          (27+strlen(S_PLOT_FILE)+strlen(ofile_name)) );
    sprintf(graf, "gnuplot -e 'signal=\"%s\"' -p " S_PLOT_FILE, ofile_name);
  }

  system(graf);

}

