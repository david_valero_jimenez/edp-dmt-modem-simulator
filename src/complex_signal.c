#include "complex_signal.h"


csignal *csignal_initialize(const unsigned int size) {
  csignal *p;

  p = csignal_alloc(size);

  if(p) {
    csignal_zero(p);
  }

  return p;
}


csignal *csignal_clone(csignal *p) {
  csignal *c;
  int n;

  c = csignal_alloc(p->size);

  if(c) {
    for(n=0;n<c->size;n++) {
      c->real[n] = p->real[n];
      c->imag[n] = p->imag[n];
    }
    c->type = p->type;
  }

  return c;
}


int csignal_copy(csignal *dest, csignal *src) {
  int n;
  int error = 1;

  if(dest && src) {

    if((dest->size!=src->size) || (dest->type!=src->type)) return error;

    if(dest->type==REAL_T) {

      for(n=0;n<dest->size;n++) {
        dest->real[n] = src->real[n];
      }

    } else {

      for(n=0;n<dest->size;n++) {
        dest->real[n] = src->real[n];
        dest->imag[n] = src->imag[n];
      }

    }

    error = 0;

  }

  return error;
}


void csignal_zero(csignal *p) {
  int n;

  if(p) {

    if(p->type==REAL_T) {

      for(n=0;n<p->size;n++) {
        p->real[n] = 0;
      }

    } else {

      for(n=0;n<p->size;n++) {
        p->real[n] = 0;
        p->imag[n] = 0;
      }

    }

  }

}


void csignal_noise(csignal *p, const double NOISE_MAX_AMP) {
  static int initialized = 0;
  int n;

  if(!initialized) {
    srand(time(NULL));
    initialized = 1;
  }

  if(p) {

    if(p->type==REAL_T) {

      for(n=0;n<p->size;n++) {
        p->real[n] =
          2*NOISE_MAX_AMP * ( (double)rand()/RAND_MAX ) - NOISE_MAX_AMP;
      }

    } else {

      for(n=0;n<p->size;n++) {
        p->real[n] =
          2*NOISE_MAX_AMP * ( (double)rand()/RAND_MAX ) - NOISE_MAX_AMP;
        p->imag[n] =
          2*NOISE_MAX_AMP * ( (double)rand()/RAND_MAX ) - NOISE_MAX_AMP;
      }

    }

  }

}


csignal *csignal_alloc(const unsigned int size) {
  csignal *p;

  p = (csignal *)malloc(sizeof(csignal));
  if(p) {
    p->real = (double *)malloc(sizeof(csignal)*size);
    p->imag = (double *)malloc(sizeof(csignal)*size);
    p->size = size;
    p->type = COMPLEX_T;
  }

  return p;
}


void csignal_free(csignal *p) {
  free(p->real);
  free(p->imag);
  free(p);
}

