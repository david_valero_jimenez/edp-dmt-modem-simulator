#include "simulation.h"


void transmission_process(simulation_data *data, int verbose) {

  /* Data summary */
  if(verbose) summary(data, TX);

  /* X(k) matrix computing */
  if(verbose) printf("\nGenerating spectrum from frame...\n\n");
  frame_to_spectrum(data);

  /* Time domain data computing (IDFT) */
  switch(data->dft_alg) {
  case DFT:
    if(verbose) printf("IDFT computing...\n\n");
    data->x = idft(data->X, data->norm_method);
    break;
  case FFTR2:
    if(verbose) printf("IFFT radix-2 computing...\n\n");
    data->x = ifft_r2(data->X, data->norm_method);
    break;
  }

  /* Signal scaling */
  if(data->x_MAX_AMP>0) {
    scale_signal(data->x->real, data->N, data->scale);
  }

  /* Signal information */
  if(verbose) printf("\nSIGNAL INFORMATION (scale: %lg):\n",
                     data->scale!=0?data->scale:1);
  data->x_PEAK_AMP = signal_peakamp(data->x->real, data->N);
  if(verbose) printf("  Normalized time-domain signal peak amplitude: "
                     "%g V*NF\n",
                     data->x_PEAK_AMP);
  data->Ps = signal_power(data->x->real, data->N);
  if(verbose) printf("  Time-domain signal power: %g Watt·NF^2\n",
                     data->Ps);


  /* White noise signal simulation and do SNR estimation */
  if(data->NOISE_MAX_AMP>0) {
    noise_simulation(data, verbose);
  }

}


void reception_process(simulation_data *data, int verbose) {

  /* Data summary */
  if(verbose) summary(data, RX);

  /* Signal scaling */
  if(data->x_MAX_AMP>0) {
    scale_signal(data->x->real, data->N, 1/data->scale);
  }

  /* Frecuency domain data computing (DFT) */
  if(verbose) printf("\n");
  switch(data->dft_alg) {
  case DFT:
    if(verbose) printf("DFT radix-2 computing...\n\n");
    data->X = dft(data->x, data->norm_method);
    break;
  case FFTR2:
    if(verbose) printf("FFT radix-2 computing...\n\n");
    data->X = fft_r2(data->x, data->norm_method);
    break;
  }

  /* Frame computing */
  if(verbose) printf("Obtaining frame from symbols...\n\n");
  spectrum_to_frame(data);

}


void noise_simulation(simulation_data *data, int verbose) {

  if(verbose) printf("\nSIGNAL-TO-NOISE RATIO ESTIMATION:\n");
  data->noise = generate_noise(data->NOISE_MAX_AMP,
                               data->N, data->norm_method);

  data->NOISE_PEAK_AMP = signal_peakamp(data->noise->real, data->N);
  if(verbose) printf("  Normalized noise signal peak amplitude: %g V·NF\n",
                     data->NOISE_PEAK_AMP);

  data->Pn = signal_power(data->noise->real, data->N);
  if(verbose) printf("  Background noise power: %g Watt·NF^2\n", data->Pn);

  data->SNR_dB = SNR_dB(data->Ps, data->Pn);
  if(verbose) printf("  SNR_dB: %g dB\n\n", data->SNR_dB);

}

