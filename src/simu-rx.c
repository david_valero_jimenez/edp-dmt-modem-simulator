#include "simu-rx.h"


int main(int argc, char **argv) {
  simulation_data *d;

  /* Command line checking */
  cline_data cl_data;
  if(clinecheck_rx(argc, argv, &cl_data)) {
    help_rx();
    exit(1);
  }


  /* Configuration reading */
  d = read_input(&cl_data, RX);

  /* Reception process results */
  reception_process(d, 1);
  printf("\n[ RECEIVED FRAME: 0x%lX ]\n\n", d->frame.data);


  /* Free allocated memory */
  simu_free(d, 1, 1, 0);

  exit(0);
  return 0;
}

