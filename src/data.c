#include "data.h"


simulation_data *read_input(cline_data *cl_data, int rx_tx) {
  simulation_data *data;
  double INC_f0_aprox;
  FILE *infile;
  int error = 0;


  data = (simulation_data *)malloc(sizeof(simulation_data));


  /* COMAND LINE CONFIGURATION DATA ASIGNMENT */
  data->dft_alg = cl_data->dft_alg;
  data->norm_method = cl_data->norm_method;
  data->p2_factor = cl_data->p2_factor;
  data->x_MAX_AMP = cl_data->x_MAX_AMP;
  data->NOISE_MAX_AMP = cl_data->NOISE_MAX_AMP;


  if( (cl_data->input_file==NULL) && (rx_tx==TX) ) {
    /* KEYBOARD CONFIGURATION INPUT */

    printf("\nEnter configuration data:\n\n");

    printf("Frame data in one of the following formats:\n");
    printf("\t0<octal_number>\n");
    printf("\t<decimal_number>\n");
    printf("\t0x<hexadecimal_number>\n\n");
    printf(": ");
    scanf("%li", &(data->frame.data));

    printf("\nNumber of subcarriers: ");
    scanf("%i", &(data->N_CARRIERS));

    printf("\nNumber of amplitudes: ");
    scanf("%i", &(data->N_AMPS));

    printf("\nNumber of phases: ");
    scanf("%i", &(data->N_PHASES));

    printf("\nSampling frequency (Hz): ");
    scanf("%lg", &(data->fs));

    printf("\nSpectral resolution, will be adapted to proper DFT size (Hz): ");
    scanf("%lg", &INC_f0_aprox);

    error = compute_config_data(data, rx_tx, NULL, INC_f0_aprox);


  } else if( ( infile=fopen(cl_data->input_file,"r") )!=NULL ) {
    /* FILE CONFIGURATION INPUT */

    /* Frame data reading (only TX case) */
    if(rx_tx==TX) {
      fscanf(infile, "%li", &(data->frame.data));
    }

    fscanf(infile, "%i", &(data->N_CARRIERS));
    fscanf(infile, "%i", &(data->N_AMPS));
    fscanf(infile, "%i", &(data->N_PHASES));
    fscanf(infile, "%lg", &(data->fs));
    fscanf(infile, "%lg", &(INC_f0_aprox));

    error = compute_config_data(data, rx_tx, infile, INC_f0_aprox);

    fclose(infile);

  } else {
    perror("readInput");
    error = 1;
  }


  if(error!=0) {
    exit(error);
  }

  return data;

}


int compute_config_data(simulation_data *data, int rx_tx,
                FILE *infile, double INC_f0_aprox) {
  int error = 0;
  unsigned int n_codes, N_aprox, read_samples = 0;
  double aux, offset, *AMPS, *PHASES;
  int i, j, k;


  /* Basic checking */
  if(data->N_CARRIERS<0) {
    error=1;
    fprintf(stderr, "[!] The number of carriers must be greater than 0.\n");
  }
  if(data->N_AMPS<0) {
    error=1;
    fprintf(stderr, "[!] The number of amplitudes must be greater than 0.\n");
  }
  if(data->N_PHASES<0) {
    error=1;
    fprintf(stderr, "[!] The number of phases must be greater than 0.\n");
  }
  if(data->fs<=0) {
    error=1;
    fprintf(stderr, "[!] Sampling frequency must be greater than 0.\n");
  }
  if(INC_f0_aprox<=0) {
    error=1;
    fprintf(stderr, "[!] Spectral resolution must be greater than 0.\n");
  }
  if((data->x_MAX_AMP<0)) {
    error=1;
    fprintf(stderr, "[!] Maximum time-domain amplitude must be greater than 0.\n");
  }


  if(!error) {

    /* Matrices allocation */
    n_codes = data->N_AMPS*data->N_PHASES;
    data->amp = (double *)malloc(n_codes*sizeof(double));
    data->freq = (double *)malloc(data->N_CARRIERS*sizeof(double));
    data->phase = (double *)malloc(n_codes*sizeof(double));


    /* Spectral resolution adaptation */
    N_aprox = data->fs / INC_f0_aprox;
    data->N = pow( 2, ceil(log(N_aprox)/M_LN2) );
    data->INC_f0 = data->fs / data->N;
    if( data->INC_f0 != INC_f0_aprox ) {
      printf("[!] Spectral resolution adapted to %lg Hz.\n", data->INC_f0);
    }


    /* Variables calculation */

    data->Ts = 1/data->fs;

    data->T0 = 1/data->INC_f0;

    data->frame.TUPLE_SIZE = ceil(log(n_codes)/M_LN2);
    data->frame.TUPLE_MASK =
      ( ((0x01<<(data->frame.TUPLE_SIZE-1))-1) << 1 ) + 1;

    data->frame.SIZE = data->N_CARRIERS*data->frame.TUPLE_SIZE;
    data->frame.MASK =
      ( (((FRAME_DATA_BASE_TYPE)0x01<<(data->frame.SIZE-1))-1) << 1 ) + 1;



    /* Frame size checking */
    if( data->frame.SIZE > 8*sizeof(FRAME_DATA_BASE_TYPE) ) {
      error = 1;
      fprintf(stderr, "[!] Specified frame size is %i bits while used type "
                      "size is %i bits.\n",
              data->frame.SIZE, 8*sizeof(FRAME_DATA_BASE_TYPE));
    }

    /* Coherence of codification states number and frame data size checking */
    if( ( data->frame.data & ~(data->frame.MASK) ) != 0 ) {
      error = 1;
      fprintf(stderr, "[!] Number of codification states is insufficient for "
                      "frame data input.\n");
    }



    /* Matrix allocation and reading of discrete samples (only RX case) */
    if(rx_tx==RX) {
      data->x = csignal_initialize(data->N);
      double *xr = data->x->real, *xi = data->x->imag;
      for( i=0; (i<data->N) && !error ; i++ ) {
        if(fscanf(infile, "%lg", xr) == 1) {     
          read_samples++;
          *xi = 0;
        } else {
          error = 1;
        }
        xr++;
        xi++;
      }
      if(read_samples!=data->N) {
        fprintf(stderr, "[!] Invalid input file format.\n");
      }
    }


    /* Frequency values computing (index 0: maximum frequency) */
    for(i=1;i<=data->N_CARRIERS;i++) {
      data->freq[i-1] = i*data->INC_f0;
    }
    /* Nyquist sampling criterion checking */
    if( data->fs < 2*data->freq[data->N_CARRIERS-1] ) {
      error = 1;
      fprintf(stderr, "[!] Sampling frequency must be at least %lg Hz: "
                      "two times the maximum frequency used (%lg Hz).\n",
              2*data->freq[data->N_CARRIERS-1],
              data->freq[data->N_CARRIERS-1]);
    }


    /* Amplitude values computing (as multiples of one) */
    AMPS = (double*)malloc(sizeof(double)*data->N_AMPS);
    for(i=1;i<=data->N_AMPS;i++) {
      AMPS[i-1] = i;
    }


    /* Normalization factor calculation (scale) */
    if(data->x_MAX_AMP!=0) {
      double limit_superior =
        SIGNAL_LIMITSUP(AMPS[data->N_AMPS-1], data->N_CARRIERS);
      data->scale = data->x_MAX_AMP/limit_superior;
      if(data->p2_factor) {
        data->scale = log(data->scale)/M_LN2;
        data->scale = data->scale<0 ? ceil(data->scale) : floor(data->scale);
        data->scale = pow(2, data->scale);
      }
    }


    /* Phase values computing */
    PHASES = (double*)malloc(sizeof(double)*data->N_PHASES);
    if(data->N_PHASES>2) {
      offset = M_PI/data->N_PHASES;
      aux = 2*offset;
      for(i=0;i<data->N_PHASES/2;i++) {
        PHASES[i] = offset+aux*i;
      }
      for(i=data->N_PHASES-1;i>=data->N_PHASES/2;i--) {
        PHASES[i] = -PHASES[data->N_PHASES-1-i];
      }
    } else if(data->N_PHASES==2) {
      PHASES[0] = 0;
      PHASES[1] = M_PI;
    }


    /* Assignment of symbol codification matrices */
    i=0;
    for(j=0;j<data->N_AMPS;j++) {
      for(k=0;k<data->N_PHASES;k++) {
        data->amp[i] = AMPS[j];
        data->phase[i] = PHASES[k];
        i++;
      }
    }

    free(AMPS);
    free(PHASES);

  }


  printf("\n");


  return error;

}


void write_output(char *ofile_name, simulation_data *d) {
  FILE *outfile;
  int n;
  double oR;
  double *pR = d->x->real, *noise;

  if( (outfile=fopen(ofile_name,"w"))!=NULL ) {

    fprintf(outfile, "%i\n%i\n%i\n%lf\n%lf\n",
            d->N_CARRIERS, d->N_AMPS, d->N_PHASES, d->fs, d->INC_f0);

    if(d->NOISE_PEAK_AMP>0) {
      noise = d->noise->real;
    }

    /* Time-domain signal writing */
    for(n=0;n<d->N;n++) {
      oR = pR[n];
      if(d->NOISE_PEAK_AMP>0) {
        oR +=  noise[n];
      }
      fprintf(outfile, "%lf\n", oR);
    }

    fclose(outfile);

  } else {

    perror("writeOutput");
    exit(1);

  }

  printf("\nResults written to: \"%s\"\n\n", ofile_name);
}


void summary(simulation_data *d, int rx_tx) {
  int i;
  char bits[d->frame.SIZE];
  FRAME_DATA_BASE_TYPE aux = d->frame.data;

  printf("INPUT DATA:\n");

  if(rx_tx == TX) {
    printf("  Frame: 0x%lX (& 0x%lX) [  ", d->frame.data, d->frame.MASK);
    for(i=0;i<d->frame.SIZE;i++) {bits[i]='0'+(aux&1);aux>>=1;};
    for(i=d->frame.SIZE-1;i>=0;--i) {
      printf("%c ", bits[i]);
      if(i%d->frame.TUPLE_SIZE==0) printf(" ");
    }
    printf("]\n\n");
  }

  printf("  Number of subcarriers: %i\n", d->N_CARRIERS);
  printf("  Number of amplitudes: %i\n", d->N_AMPS);
  printf("  Number of phases: %i\n\n", d->N_PHASES);

  printf("  Sampling frequency (f_s): %.1lf Hz\n", d->fs);
  printf("  Spectral resolution (INC. f_0): %.1lf Hz\n", d->INC_f0);

  if(rx_tx == RX) {
    printf("  Number of time-domain samples read: %i\n", d->N);
  }


  printf("\n\nPROCESSED DATA:\n");
  printf("  Frame mask: 0x%lX\n", d->frame.MASK);
  printf("  Frame size: %i bits\n", d->frame.SIZE);
  printf("  Tuple size: %i bits\n\n", d->frame.TUPLE_SIZE);

  printf("  Subcarriers (Hz):");
  for(i=0;i<d->N_CARRIERS;i++) {printf(" %.1lf", d->freq[i]);};
  printf("\n\n");

  printf("  Ts: %lg s\n", d->Ts);
  printf("  T0: %lg s\n\n", d->T0);

  printf("  DFT adjusted size: %i\n\n", d->N);

}


simulation_data *simu_clone(simulation_data *p) {
  simulation_data *c;
  int i;
  unsigned int n_codes = p->N_AMPS * p->N_PHASES;

  c = (simulation_data*)malloc(sizeof(simulation_data));

  c->x = csignal_clone(p->x);
  c->X = csignal_clone(p->X);
  c->noise = csignal_clone(p->noise);

  c->amp = (double*)malloc(sizeof(double)*n_codes);
  c->freq = (double*)malloc(sizeof(double)*p->N_CARRIERS);
  c->phase = (double*)malloc(sizeof(double)*n_codes);

  c->fs = p->fs;
  c->INC_f0 = p->INC_f0;
  c->Ts = p->Ts;
  c->T0 = p->T0;
  c->frame = p->frame;
  c->N_CARRIERS = p->N_CARRIERS;
  c->N_AMPS = p->N_AMPS;
  c->N_PHASES = p->N_PHASES;
  c->N = p->N;
  c->norm_method = p->norm_method;
  c->p2_factor = p->p2_factor;
  c->scale = p->scale;
  c->dft_alg = p->dft_alg;
  c->NOISE_MAX_AMP = p->NOISE_MAX_AMP;
  c->NOISE_PEAK_AMP = p->NOISE_PEAK_AMP;
  c->x_MAX_AMP = p->x_MAX_AMP;
  c->x_PEAK_AMP = p->x_PEAK_AMP;
  c->Ps = p->Ps;
  c->Pn = p->Pn;
  c->SNR_dB = p->SNR_dB;

  for(i=0;i<n_codes;i++) c->amp[i] = p->amp[i];
  for(i=0;i<p->N_CARRIERS;i++) c->freq[i] = p->freq[i];
  for(i=0;i<n_codes;i++) c->phase[i] = p->phase[i];

  return c;
}


void simu_free(simulation_data *p, int fx, int fX, int fnoise) {
  free(p->amp);
  free(p->freq);
  free(p->phase);
  if(fx) csignal_free(p->x);
  if(fX) csignal_free(p->X);
  if(fnoise) csignal_free(p->noise);
  free(p);
}

