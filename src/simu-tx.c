#include "simu-tx.h"


int main(int argc, char **argv) {
  simulation_data *d;

  /* Command line checking */
  cline_data cl_data;
  if(clinecheck_tx(argc, argv, &cl_data)) {
    help_tx();
    exit(1);
  }


  /* Configuration and frame reading */
  d = read_input(&cl_data, TX);

  /* Transmission process results */
  transmission_process(d, 1);

  /* Output generation */
  write_output(cl_data.output_file, d);

  /* Plot generation */
  if(cl_data.plot) {
    generate_plot(cl_data.output_file, d);
  }


  /* Free allocated memory */
  simu_free(d, 1, 1, d->NOISE_MAX_AMP>0?1:0);

  exit(0);
  return 0;
}

