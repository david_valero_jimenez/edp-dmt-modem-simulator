#include "cli.h"


int clinecheck(int argc, char **argv, cline_data *cl_data) {
  int error = 0;
  char **cp_aux;


  while( (--argc>0) && !error ) {

    argv++;

    if(**argv=='-') {

      (*argv)++;

      switch(**argv) {


      case 'i':
      case 'o':
        /* Input and output files */

        switch(**argv) {
        case 'i':
          cp_aux = &(cl_data->input_file);
          break;
        case 'o':
          cp_aux = &(cl_data->output_file);
          break;
        }

        (*argv)++;
        if(**argv!='\0') {
          error=1;
        } else {
          argv++;
          argc--;
        }

        *cp_aux = *argv;

        break;


      case 'a':
        /* DFT algorithm */

        (*argv)++;
        if( (**argv!='\0') || (argc<2) ) {
          error=1;
        } else {

          argv++;
          argc--;

          if(strcmp(*argv,"dft")==0) {
            cl_data->dft_alg = DFT;
          } else if(strcmp(*argv,"fft")==0) {
            cl_data->dft_alg = FFTR2;
          } else {
            error = 1;
          }

        }

        break;


      case 'f':
        /* DFT normalization method (normalization factor) */

        (*argv)++;
        if( (**argv!='\0') || (argc<2) ) {
          error=1;
        } else {

          argv++;
          argc--;

          if(strcmp(*argv,"unitary")==0) {
            cl_data->norm_method = UNITARY;
          } else if(strcmp(*argv,"direct")==0) {
            cl_data->norm_method = DIVN_DIRECT;
          } else if(strcmp(*argv,"inverse")==0) {
            cl_data->norm_method = DIVN_INVERSE;
          } else if( (strcmp(*argv,"force")==0) && (argc>1) ) {
            cl_data->norm_method = DIVN_DIRECT;
            argc--;
            argv++;
            sscanf(*argv, "%lg", &(cl_data->x_MAX_AMP));
          } else if( (strcmp(*argv,"power2")==0) && (argc>1) ) {
            cl_data->norm_method = DIVN_DIRECT;
            cl_data->p2_factor = 1;
            argc--;
            argv++;
            sscanf(*argv, "%lg", &(cl_data->x_MAX_AMP));
          } else {
            error = 1;
          }

        }

        break;


      case 'n':
        /* Noise peak amplitude */

        (*argv)++;
        if(**argv!='\0') {
          error=1;
        } else {

          argv++;
          argc--;

          sscanf(*argv, "%lg", &(cl_data->NOISE_MAX_AMP));

        }

        break;


      case 'p':
        /* Plot results */

        (*argv)++;
        if(**argv!='\0') {
          error=1;
        } else {
          cl_data->plot = 1;
        }

        break;


      case 'h':
        /* Help */

        error = 1;

        break;


      case 't':
        /* Number of tests */

        (*argv)++;
        if(**argv!='\0') {
          error=1;
        } else {

          argv++;
          argc--;

          sscanf(*argv, "%i", &(cl_data->N_TESTS));

        }

        break;


      }


    } else {
      error=1;
    }

  }


  return error;
}


void help_tx() {
  fprintf(stderr,"Simulation program for DMT modem transmission process.\n\n");

  fprintf(stderr, "Usage: [-i <configuration_file>] -o <output_file> "
                  "[-a <algorithim>]\n       [-f <normalization_method>] "
                  "[-n <noise_peak_amplitude>] [-p] [-h]\n\n");

  fprintf(stderr, "\tOptions:\n");
  fprintf(stderr, "\t\t-i: Optional configuration file.\n"
          "\t\t    If not specified program will ask for configuration \n"
          "\t\t    parameters.\n");
  fprintf(stderr, "\t\t-o: Required output file.\n");
  fprintf(stderr, "\t\t-a: DFT algorithm to use: dft or fft (lower case).\n"
          "\t\t    (default: fft)\n");
  fprintf(stderr, "\t\t-f: Normalization factor to use (lower case):\n"
          "\t\t      unitary:    Unitary transform.\n"
          "\t\t      direct:     Divides by N in direct transform.\n"
          "\t\t      inverse:    Divides by N in inverse transform.\n"
          "\t\t      force <M>:  Calculates normalization factor to produce\n"
          "\t\t                  a time-domain amplitude not greater than M.\n"
          "\t\t      power2 <M>: Same as force with power of two scale.\n"
          "\t\t    (default: inverse)\n");
  fprintf(stderr, "\t\t-n: Noise maximum amplitude (V). (default: 0)\n");
  fprintf(stderr, "\t\t-p: If specified, program will plot results "
                  "(gnuplot required).\n");
  fprintf(stderr, "\t\t-h: This help message.\n\n");

  fprintf(stderr, "\tConfiguration file format:\n");
  fprintf(stderr, "\t\t<frame>  ( dec | hex | oct )\n");
  fprintf(stderr, "\t\t<number_of_subcarriers>\n");
  fprintf(stderr, "\t\t<number_of_amplitude_values>\n");
  fprintf(stderr, "\t\t<number_of_phase_values>\n");
  fprintf(stderr, "\t\t<sampling_frequency> (Hz)\n");
  fprintf(stderr, "\t\t<spectral_resolution> (will be adapted) (Hz)\n\n");

  fprintf(stderr, CREDITS);
}


int clinecheck_tx(int argc, char **argv, cline_data *cl_data) {
  int error = 0;

  if(argc==1) return 1;

  cl_data->input_file = NULL;
  cl_data->output_file = NULL;
  cl_data->dft_alg = FFTR2;
  cl_data->norm_method = DIVN_INVERSE;
  cl_data->p2_factor = 0;
  cl_data->x_MAX_AMP = 0;
  cl_data->NOISE_MAX_AMP = 0;
  cl_data->plot = 0;
  cl_data->N_TESTS = -1;

  error = clinecheck(argc, argv, cl_data);


  if( !error ) {

    if( cl_data->NOISE_MAX_AMP < 0 ) {
      fprintf(stderr,
        "[!] Noise maximum amplitude must be equal to or greater than 0.\n");
      error = 1;
    }

  }


  if( (cl_data->output_file==NULL) || (cl_data->N_TESTS!=-1) ) {
    error = 1;
  }


  return error;
}


void help_rx() {
  fprintf(stderr, "Simulation program for DMT modem reception process.\n\n");

  fprintf(stderr, "Use: -i <input_file> [-a <algorithim>] "
                  "[-f <normalization_factor>] [-h]\n\n");

  fprintf(stderr, "\tOptions:\n");
  fprintf(stderr, "\t\t-i: Required input file.\n");
  fprintf(stderr, "\t\t-a: DFT algorithm to use: dft or fft (lower case).\n"
          "\t\t    (default: fft)\n");
  fprintf(stderr, "\t\t-f: Normalization factor to use (lower case):\n"
          "\t\t      unitary:    Unitary transform.\n"
          "\t\t      direct:     Divides by N in direct transform.\n"
          "\t\t      inverse:    Divides by N in inverse transform.\n"
          "\t\t      force <M>:  Calculates normalization factor to produce\n"
          "\t\t                  a time-domain amplitude not greater than M.\n"
          "\t\t      power2 <M>: Same as force with power of two scale.\n"
          "\t\t    (default: inverse)\n");
  fprintf(stderr, "\t\t-h: This help message.\n\n");

  fprintf(stderr, "\tInput file format (as generated by transmission "
          "program):\n");
  fprintf(stderr, "\t\t<number_of_subcarriers>\n");
  fprintf(stderr, "\t\t<number_of_amplitude_values>\n");
  fprintf(stderr, "\t\t<number_of_phase_values>\n");
  fprintf(stderr, "\t\t<sampling_frequency> (Hz)\n");
  fprintf(stderr, "\t\t<spectral_resolution> (will be adapted) (Hz)\n");
  fprintf(stderr, "\t\t<xR(0)>\n");
  fprintf(stderr, "\t\t<xR(1)>\n");
  fprintf(stderr, "\t\t...\n");
  fprintf(stderr, "\t\t<xR(N-1)>\n\n");

  fprintf(stderr, CREDITS);
}


int clinecheck_rx(int argc, char **argv, cline_data *cl_data) {
  int error = 0;

  if(argc==1) return 1;

  cl_data->input_file = NULL;
  cl_data->output_file = NULL;
  cl_data->dft_alg = FFTR2;
  cl_data->norm_method = DIVN_INVERSE;
  cl_data->p2_factor = 0;
  cl_data->x_MAX_AMP = 0;
  cl_data->NOISE_MAX_AMP = -1;
  cl_data->plot = 0;
  cl_data->N_TESTS = -1;

  error = clinecheck(argc, argv, cl_data);

  if( (cl_data->input_file==NULL) || (cl_data->output_file!=NULL) ||
      (cl_data->NOISE_MAX_AMP!=-1) || (cl_data->plot!=0) ||
      (cl_data->N_TESTS!=-1) ) {
    error = 1;
  }

  return error;
}


void help_stats() {
  fprintf(stderr, "Simulation statistics program for DMT modem.\n\n");

  fprintf(stderr, "Usage: -t <number_of_tests> -n <noise_peak_amplitude> "
                  "[-i <configuration_file>]\n       [-a <algorithim>] "
                  "[-f <normalization_method>] [-h]\n\n");

  fprintf(stderr, "\tOptions:\n");
  fprintf(stderr, "\t\t-t: Number of tests to run.\n");
  fprintf(stderr, "\t\t-n: Noise maximum amplitude (V).\n");
  fprintf(stderr, "\t\t-i: Optional configuration file.\n"
          "\t\t    If not specified program will ask for configuration\n"
          "\t\t    parameters.\n");
  fprintf(stderr, "\t\t-a: DFT algorithm to use: dft or fft (lower case).\n"
          "\t\t    (default: fft)\n");
  fprintf(stderr, "\t\t-f: Normalization factor to use (lower case):\n"
          "\t\t      unitary:    Unitary transform.\n"
          "\t\t      direct:     Divides by N in direct transform.\n"
          "\t\t      inverse:    Divides by N in inverse transform.\n"
          "\t\t      force <M>:  Calculates normalization factor to produce\n"
          "\t\t                  a time-domain amplitude not greater than M.\n"
          "\t\t      power2 <M>: Same as force with power of two scale.\n"
          "\t\t    (default: inverse)\n");
  fprintf(stderr, "\t\t-h: This help message.\n\n");

  fprintf(stderr, "\tConfiguration file format:\n");
  fprintf(stderr, "\t\t<frame>  ( dec | hex | oct )\n");
  fprintf(stderr, "\t\t<number_of_subcarriers>\n");
  fprintf(stderr, "\t\t<number_of_amplitude_values>\n");
  fprintf(stderr, "\t\t<number_of_phase_values>\n");
  fprintf(stderr, "\t\t<sampling_frequency> (Hz)\n");
  fprintf(stderr, "\t\t<spectral_resolution> (will be adapted) (Hz)\n\n");

  fprintf(stderr, CREDITS);
}


int clinecheck_stats(int argc, char **argv, cline_data *cl_data) {
  int error = 0;

  if(argc==1) return 1;

  cl_data->input_file = NULL;
  cl_data->output_file = NULL;
  cl_data->dft_alg = FFTR2;
  cl_data->norm_method = DIVN_INVERSE;
  cl_data->p2_factor = 0;
  cl_data->x_MAX_AMP = 0;
  cl_data->NOISE_MAX_AMP = 0;
  cl_data->plot = 0;
  cl_data->N_TESTS = 0;

  error = clinecheck(argc, argv, cl_data);


  if( !error ) {

    if( cl_data->NOISE_MAX_AMP <= 0 ) {
      fprintf(stderr,
        "[!] Noise maximum amplitude must be specified and greater than 0.\n");
      error = 1;
    }

    if( cl_data->N_TESTS < 1 ) {
      fprintf(stderr,
        "[!] Number of tests expected to be equal or greater than one.\n");
      error = 1;
    }

  }


  if( (cl_data->output_file!=NULL) || (cl_data->plot!=0) ) {
    error = 1;
  }

  return error;
}

