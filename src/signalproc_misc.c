#include "signalproc_misc.h"


csignal *generate_noise(double NOISE_MAX_AMP, const unsigned int N,
                        normalization_type norm_method) {

  csignal *noise;

  noise = csignal_alloc(N);
  noise->type = REAL_T;
  csignal_noise(noise, NOISE_MAX_AMP);

  normalize(noise, 1, norm_method);

  return noise;

}


double signal_power(double *s, unsigned int N) {
  double power = 0;
  double *sp = s+N;

  do {
    sp--;
    power += (*sp * *sp)/N;
  } while(sp > s);

  return power;
}


double signal_peakamp(double *s, unsigned int N) {
  int n;
  double peak = DBL_MIN, sample;

  for(n=0;n<N;n++) {
    sample = fabs(s[n]);
    if( sample > peak ) {
      peak = sample;
    }
  }

  return peak;
}


void scale_signal(double *s, unsigned N, double scale) {
  int i;
  for(i=0;i<N;i++) {
    s[i]*=scale;
  }
}
