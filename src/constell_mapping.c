#include "constell_mapping.h"


void frame_to_spectrum(simulation_data *d) {
  unsigned int symbol;
  int  i, k;

  /* Generate spectrum */
  d->X = csignal_initialize(d->N);

  for(i=0;i<d->N_CARRIERS;i++) {

    k = i + 1;
    symbol = tuple(d->frame, i);

    d->X->real[k] = d->amp[symbol] * cos(d->phase[symbol]);
    d->X->imag[k] = d->amp[symbol] * sin(d->phase[symbol]);
    /* printf("%i) %lf + %lf i\n",symbol,d->X->real[k],d->X->imag[k]); */
    if( k != (d->N/2) ) {
      d->X->real[d->N-k] = d->X->real[k];
      d->X->imag[d->N-k] = -d->X->imag[k];
    }

  }

}


void spectrum_to_frame(simulation_data *d) {
  int n, i;
  unsigned int n_codes;
  double **symbol, diff, min_diff;
  FRAME_DATA_BASE_TYPE match;


  /* Precompute reference symbols */
  n_codes = d->N_AMPS*d->N_PHASES;
  symbol = (double**)malloc(2*sizeof(double*));
  symbol[REAL] = (double*)malloc(n_codes*sizeof(double));
  symbol[IMAG] = (double*)malloc(n_codes*sizeof(double));
  for(i=0;i<n_codes;i++) {
    symbol[REAL][i] = d->amp[i] * cos(d->phase[i]);
    symbol[IMAG][i] = d->amp[i] * sin(d->phase[i]);
  }

  /* Generate frame */
  d->frame.data = 0;

  for(n=1;n<=d->N_CARRIERS;n++) {

    match = LONG_MAX;
    min_diff = DBL_MAX;

    for(i=0;i<n_codes;i++) {

      diff = fabsf( symbol[REAL][i] - d->X->real[n] )
                    + fabsf( symbol[IMAG][i] - d->X->imag[n] );

      if( diff < min_diff ) {
        min_diff = diff;
        match = i;
      }

    }

    /* Optional amplitude and phase calculation */
    //double a = sqrt( d->X.real[n]*d->X.real[n] + d->X.imag[n]*d->X.imag[n] );
    //double p = ( asin(d->X.imag[n])<0 ? -1 : 1 ) * 180*acos(d->X.real[n]/a) / M_PI;
    //fprintf(stderr, "%i) AMP: %lf  PHASE: %lf\n", n, a, p);

    d->frame.data |= match<<(d->frame.TUPLE_SIZE*(n-1));
  }

}

