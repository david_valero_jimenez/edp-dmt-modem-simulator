#include "fft.h"


csignal *fft_r2(csignal *x, normalization_type norm_method) {
  return _fft_ifft_r2(x, 0, norm_method);
}

csignal *ifft_r2(csignal *X, normalization_type norm_method) {
  return _fft_ifft_r2(X, 1, norm_method);
}

csignal *_fft_ifft_r2(csignal *input, int inv,
                      normalization_type norm_method) {

  if(input==NULL) return NULL;

  unsigned int N = input->size;
  double  WN__, WN_, **WN, *WNr, *WNi;
  csignal *output, *input_br, *swap;
  double *o1R, *o1I, *o2R, *o2I, *i1R, *i1I, *i2R, *i2I;
  double mult[2];
  int i, b, k;
  int S, B, Nb, MED = N>>1, MEDb;


  /* PHASE FACTOR COMPUTING */
  WN__ = -2*M_PI/N;
  WN__ = inv?-WN__:WN__;  /* IFFT */
  WN = (double **)malloc(sizeof(double *)*2);  /* k={0,...,N/2-1} */
  WN[REAL] = (double *)malloc(sizeof(double)*MED);
  WN[IMAG] = (double *)malloc(sizeof(double)*MED);
  for(k=0;k<MED;k++) {
    WN_ = WN__ * k;
    WN[REAL][k] = cos(WN_);
    WN[IMAG][k] = sin(WN_);
  }


  /* Input bitreverse ordering */
  input_br = csignal_clone(input);
  bit_reverse(input_br);

  /* Input and output matrices will swap in each stage */
  output = csignal_alloc(N);


  /*
    S: number of stages
    B: number of butterflies in each stage
    Nb: size of butterfly in each stage
  */
  S = round(log(N)/M_LN2);
  B = MED;
  Nb = N/B;
  for(i=0;i<S;i++) {  /* for each stage */
    MEDb = Nb>>1;

    if(i%2 == 0) {  /* input and output assignment */
      i1R = i2R = input_br->real; i1I = i2I = input_br->imag;
      o1R = o2R = output->real; o1I = o2I = output->imag;
    } else {
      i1R = i2R = output->real; i1I = i2I = output->imag;
      o1R = o2R = input_br->real; o1I = o2I = input_br->imag;
    }

    i2R += MEDb; i2I += MEDb;
    o2R += MEDb; o2I += MEDb;


    for(b=0;b<B;) {  /* for each butterfly */
      WNr = WN[REAL];
      WNi = WN[IMAG];

      for(k=0;k<MEDb;k++) {
        *o1R = *o2R = *i1R;
        *o1I = *o2I = *i1I;

        if(k==0) {
          *o1R += *i2R; *o1I += *i2I;
          *o2R -= *i2R; *o2I -= *i2I;
        } else {
          mult[REAL] = *WNr * *i2R - *WNi * *i2I;
          mult[IMAG] = *WNr * *i2I + *WNi * *i2R;
          *o1R += mult[REAL]; *o1I += mult[IMAG];
          *o2R -= mult[REAL]; *o2I -= mult[IMAG];
        }

        i1R++; i1I++; i2R++; i2I++;
        o1R++; o1I++; o2R++; o2I++;

        WNr+=B; WNi+=B;
      }

      b++;
      i1R+=MEDb; i1I+=MEDb; i2R+=MEDb; i2I+=MEDb;
      o1R+=MEDb; o1I+=MEDb; o2R+=MEDb; o2I+=MEDb;

    }

    B = B>>1;
    Nb = Nb<<1;

  }

  if(S%2 == 0) {  /* swap input_br and output if needed */
    swap = output;
    output = input_br;
    input_br = swap;
  }

  /* Normalize output */
  normalize(output, inv, norm_method);


  csignal_free(input_br);
  free(WN[REAL]);
  free(WN[IMAG]);
  free(WN);


  return output;
}


void bit_reversed_index(unsigned int *bri, unsigned int N) {
  /* 
    By Sean Eron Anderson. See fft.h header file for details.
  */

  unsigned int i;

  static const unsigned char bit_reverse_table256[256] = {
#define R2(n)     n,     n + 2*64,     n + 1*64,     n + 3*64
#define R4(n) R2(n), R2(n + 2*16), R2(n + 1*16), R2(n + 3*16)
#define R6(n) R4(n), R4(n + 2*4 ), R4(n + 1*4 ), R4(n + 3*4 )
    R6(0), R6(2), R6(1), R6(3)
  };

  for(i=0;i<N;i++) {
    unsigned char * p = (unsigned char *) &i;
    unsigned char * q = (unsigned char *) (bri+i);

    q[3] = bit_reverse_table256[p[0]];
    q[2] = bit_reverse_table256[p[1]];
    q[1] = bit_reverse_table256[p[2]];
    q[0] = bit_reverse_table256[p[3]];

    /* Fix position */
    int s = sizeof(int)*8-round(log(N-1)/M_LN2);
    *(bri+i) = *(bri+i)>>s;
  }

}


void bit_reverse(csignal *input) {
  unsigned int N = input->size;
  csignal *output;
  double *oR, *oI, *iR, *iI;
  int k, i;

  unsigned int *bri = (unsigned int *)malloc(sizeof(unsigned int)*N);
  bit_reversed_index(bri, N);

  output = csignal_alloc(N);

  iR = input->real;
  iI = input->imag;
  oR = output->real;
  oI = output->imag;

  for(k=0;k<N;k++) {
    i = bri[k];
    *oR = iR[i];
    *oI = iI[i];
    oR++; oI++;
  }

  csignal_copy(input, output);

  free(bri);
  csignal_free(output);

}

