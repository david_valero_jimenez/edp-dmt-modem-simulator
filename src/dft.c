#include "dft.h"


csignal *dft(csignal *x, normalization_type norm_method) {
  return _dft_idft(x, 0, norm_method);
}

csignal *idft(csignal *X, normalization_type norm_method) {
  return _dft_idft(X, 1, norm_method);
}

csignal *_dft_idft(csignal *input, int inv,
                   normalization_type norm_method) {

  unsigned int N = input->size;
  double  WN__, WN_, WN;
  csignal *output;
  double *oR, *oI, *iR, *iI;
  double c, s;
  int k, n;

  /* Allocate output matrix memory */
  output = csignal_alloc(N);
  oR = output->real;
  oI = output->imag;

  WN__ = -2*M_PI/N;
  WN__ = inv?-WN__:WN__;  /* IDFT */

  for(k=0;k<N;k++) {
    *oR = 0;
    *oI = 0;
    WN_ = WN__ * k;
    iR = input->real;
    iI = input->imag;
    for(n=0;n<N;n++) {
      WN = WN_ * n;
      c = cos(WN);
      s = sin(WN);
      *oR += *iR*c - *iI*s;
      *oI += *iR*s + *iI*c;
      iR++;
      iI++;
    }

    oR++;
    oI++;
  }

  /* Normalize output */
  normalize(output, inv, norm_method);

  return output;
}


void normalize(csignal *signal, int inv, normalization_type norm_method) {

  unsigned int N = signal->size;
  double factor;
  int i;

  switch(norm_method) {
  case UNITARY:
  /* Unitary transform */
    factor = 1/sqrt(N);
    break;
  case DIVN_DIRECT:
  /* Divides by N in direct transform */
    factor = inv ? 1 : 1/(double)N ;
    break;
  case DIVN_INVERSE:
  /* Divides by N in inverse transform */
    factor = inv ? 1/(double)N : 1 ;
    break;
  }

  for(i=0;i<N;i++) {
    signal->real[i] *= factor;
    if(signal->type==COMPLEX_T) {
      signal->imag[i] *= factor;
    }
  }

}

