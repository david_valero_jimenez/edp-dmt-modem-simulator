/*
 * END-OF-DEGREE PROJECT: DESIGN AND SIMULATION OF DMT MODEM
 * PROGRAM TO SIMULATE DMT RECEPTION PROCESS
 *
 * David Valero Jimenez
 * Degree in Computer Science, Intensification in Computation
 * University of Castilla - La Mancha, Ciudad Real, Spain
 */

#include <stdio.h>
#include <stdlib.h>

#include "cli.h"
#include "data.h"
#include "simulation.h"

