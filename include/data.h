#ifndef DATA_H
#define DATA_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "complex_signal.h"
#include "cli.h"
#include "signalproc_misc.h"
#include "dft.h"


#define STRING_FRAME_MAX 512

/*
  STRUCTS AND FUNCTIONS FOR MANAGEMENT OF SIMULATION DATA
*/

/* FLAGS */
#define TX 0x01
#define RX 0x02

/* First tuple (X==0) matches lower frequency (fundamental) subcarrier */
#define tuple(frame,X) \
        ( ( (frame.data)>>(frame.TUPLE_SIZE*X) ) & frame.TUPLE_MASK )

#define FRAME_DATA_BASE_TYPE unsigned long int

typedef struct {
  FRAME_DATA_BASE_TYPE data;
  FRAME_DATA_BASE_TYPE MASK;
  unsigned int SIZE;
  FRAME_DATA_BASE_TYPE TUPLE_MASK;
  unsigned int TUPLE_SIZE;
} Frame;

typedef struct {
/* Sampling freq., spectral resolution and corresponding periods */
  double fs, INC_f0, Ts, T0;
/* Frame data structure */
  Frame frame;
/* Subcarrier, amplitude and phase matrix size */
  unsigned int N_CARRIERS, N_AMPS, N_PHASES;
/* Amplitude, subcarrier freq. and phase matrices */
  double *amp, *freq, *phase;
/* DFT size */
  unsigned int N;
/* Time-domain, frequency-domain and noise samples matrices */
  csignal *x, *X, *noise;
/* Transform normalization method */
  normalization_type norm_method;
/* Boolean to indicate if normalization factor must be power of two or not */
  int p2_factor;
/* Scale of time-domain signal */
  double scale;
/* Algorithm used for DFT computation */
  DFT_algorithm dft_alg;
/* Noise peak and maximum amplitudes; Time-domain peak amplitude */
  double NOISE_MAX_AMP, NOISE_PEAK_AMP, x_MAX_AMP, x_PEAK_AMP;
/* Time-domain signal power, noise signal power and Signal-to-Noise Ratio */
  double Ps, Pn, SNR_dB;
} simulation_data;


/* Simulation data management functions */
simulation_data *read_input(cline_data *cl_data, int rx_tx);
int compute_config_data(simulation_data *d, int rx_tx, FILE *infile,
                      double fs_aprox);
void write_output(char *ofile_name, simulation_data *d);
void summary(simulation_data *d, int rx_tx);
simulation_data *simu_clone(simulation_data *p);
void simu_free(simulation_data *p, int fx, int fX, int fnoise);


#endif

