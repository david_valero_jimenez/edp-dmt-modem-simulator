#ifndef CLI_H
#define CLI_H

#include <stdio.h>
#include <string.h>

#include "dft.h"


#define CREDITS "David Valero Jimenez\n" \
                "University of Castilla - La Mancha, Ciudad Real, Spain\n\n"


typedef struct {
  char *input_file, *output_file;
  DFT_algorithm dft_alg;
  normalization_type norm_method;
  int p2_factor;
  double x_MAX_AMP, NOISE_MAX_AMP;
  int plot;
  int N_TESTS;
} cline_data;

int clinecheck(int argc, char **argv, cline_data *cl_data);
int clinecheck_tx(int argc, char **argv, cline_data *cl_data);
int clinecheck_rx(int argc, char **argv, cline_data *cl_data);
int clinecheck_stats(int argc, char **argv, cline_data *cl_data);
void help_tx();
void help_rx();
void help_stats();

#endif

