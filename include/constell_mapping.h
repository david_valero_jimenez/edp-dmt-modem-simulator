#ifndef CONSTELL_MAPPING_H
#define CONSTELL_MAPPING_H

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#include <float.h>

#include "data.h"


/* Function to obtain X(k) from frame data */
void frame_to_spectrum(simulation_data *d);

/* Function to obtain frame from X(k) data */
void spectrum_to_frame(simulation_data *d);

#endif

