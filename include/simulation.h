#ifndef SIMULATION_H
#define SIMULATION_H

#include "data.h"
#include "constell_mapping.h"
#include "dft.h"
#include "fft.h"
#include "signalproc_misc.h"


void transmission_process(simulation_data *d, int verbose);
void reception_process(simulation_data *d, int verbose);
void noise_simulation(simulation_data *d, int verbose);


#endif

