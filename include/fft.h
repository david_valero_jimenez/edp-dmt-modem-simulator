#ifndef FFT_H
#define FFT_H

#include <stdlib.h>
#include <math.h>

#include "complex_signal.h"
#include "dft.h"


/* Radix-2 Fast Fourier Transform algorithm implementation */
csignal *_fft_ifft_r2(csignal *input, int inv,
                      normalization_type norm_method);

/* Driver function for radix-2 FFT and IFFT */
csignal *fft_r2(csignal *x, normalization_type norm_method);
csignal *ifft_r2(csignal *X, normalization_type norm_method);


/* BIT-REVERSE */

/*
  bit_reversed_index(unsigned int *bri, int N) implements:

  "Reverse bits in word by lookup table"

  By Sean Eron Anderson
  seander@cs.stanford.edu

  On July 14, 2009 Hallvard Furuseth suggested the macro compacted table.

  ---

  The function initializes the unsigned integer matrix "bri" (size N)
  with the bit reversed index value of each element.
*/
void bit_reversed_index(unsigned int *bri, unsigned int N);


/*
  In-place complex matrix bit-reverse function

  This function reorders the input complex matrix by using
  *bitReversedIndex(int N) to calculate a bit-reversed index matrix of size N
*/
void bit_reverse(csignal *input);


#endif

