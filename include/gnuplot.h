#ifndef GNUPLOT_H
#define GNUPLOT_H

#include <stdio.h>
#include <string.h>

#include "data.h"


#define PLOTDIR "gnuplot"
#define NOISE_FILE PLOTDIR"/noise.dat"
#define S_PLOT_FILE PLOTDIR"/signal.plot"
#define SN_PLOT_FILE PLOTDIR"/signal_noise.plot"

void generate_plot(char *ofile_name, simulation_data *d);


#endif

