#ifndef SIGNALPROC_MISC_H
#define SIGNALPROC_MISC_H

#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <float.h>

#include "complex_signal.h"
#include "data.h"
#include "dft.h"


/*
  NOISE_PEAK_AMP: Peak amplitude of noise signal (user specified).
  N: signal matrices size.
  Ps: Power of main signal.
  Pn: Power of background noise.
*/


/* Computes snr from Ps and Pn power values*/
#define SNR(Ps, Pn) ((double) Ps / Pn )

/* Computes snr_dB from Ps and Pn power values */
#define SNR_dB(Ps,Pn) ((double) 10 * log(SNR(Ps,Pn)) )

/* Returns non-normalized limit superior estimation of time-domain signal,
   given subcarriers number and maximum subcarrier amplitude */
#define SIGNAL_LIMITSUP(MAX_AMP, N_CARRIERS) ((double) \
                                              MAX_AMP * N_CARRIERS * 2 )



/* Generates N samples of a white noise signal (normalized by given factor) */
csignal *generate_noise(double NOISE_PEAK_AMP, unsigned int N,
                       normalization_type norm_method);


/* Computes the power of N-samples "signal" */
double signal_power(double *s, unsigned int N);


/* Returns peak amplitude of given signal */
double signal_peakamp(double *s, unsigned int N);

/* Scales given N-samples signal by <scale> */
void scale_signal(double *s, unsigned int N, double scale);


#endif

