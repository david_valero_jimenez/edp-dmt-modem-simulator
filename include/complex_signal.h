#ifndef COMPLEX_SIGNAL_H
#define COMPLEX_SIGNAL_H

#include <stdlib.h>
#include <time.h>


/* Complex index constants and signal types */
#define REAL 0
#define IMAG 1

typedef enum { REAL_T, COMPLEX_T } signal_type;
typedef struct {
  unsigned int size;
  signal_type type;
  double *real;
  double *imag;
} csignal;


/* Allocates and initializes signal to zero */
csignal *csignal_initialize(const unsigned int size);

/* Clone csignal struct */
csignal *csignal_clone(csignal *p);

/* Reinitialize functions */
void csignal_zero(csignal *p);
void csignal_noise(csignal *p, const double NOISE_MAX_AMP);
int csignal_copy(csignal *dest, csignal *src);

/* Basic functions for struct memory management */
csignal *csignal_alloc(const unsigned int size);
void csignal_free(csignal *p);


#endif

