#ifndef DFT_H
#define DFT_H

#include <stdlib.h>
#include <math.h>

#include "complex_signal.h"


typedef enum {DFT, FFTR2} DFT_algorithm;
typedef enum {
  UNITARY,
  DIVN_DIRECT,
  DIVN_INVERSE
} normalization_type;


/* Direct DFT algorithm implementation */
csignal *_dft_idft(csignal *input, int inv,
                   normalization_type norm_method);

/* Driver functions for DFT and IDFT */
csignal *dft(csignal *x, normalization_type norm_method);
csignal *idft(csignal *X, normalization_type norm_method);


/* DFT normalization function */
void normalize(csignal *signal, int inv, normalization_type norm_method);


#endif

